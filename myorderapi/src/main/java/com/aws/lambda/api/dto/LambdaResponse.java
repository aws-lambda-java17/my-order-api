package com.aws.lambda.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LambdaResponse <T extends BaseData> {
    public int errorCode;
    public String message;
    public T data;
    public List<T> list;
    public LambdaResponse() {}

    public LambdaResponse(int errorCode, String message, T data) {
        this.errorCode = errorCode;
        this.message = message;
        this.data = data;
        this.list = null;
    }

    public LambdaResponse(int errorCode, String message, List<T> list) {
        this.errorCode = errorCode;
        this.message = message;
        this.data = null;
        this.list = list;
    }
}


