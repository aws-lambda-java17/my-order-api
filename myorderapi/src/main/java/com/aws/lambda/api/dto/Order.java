package com.aws.lambda.api.dto;

public class Order implements BaseData {
    public String id;
    public String itemName;
    public int quantity;

    public Order() {}
    public Order(String id, String itemName, int quantity) {
        this.id = id;
        this.itemName = itemName;
        this.quantity = quantity;
    }
}

