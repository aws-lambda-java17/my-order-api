package com.aws.lambda.api.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.aws.lambda.api.dto.LambdaResponse;
import com.aws.lambda.api.dto.Order;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.UUID;

public class CreateOrderHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent apiGatewayProxyRequestEvent, Context context) {
        ObjectMapper mapper = new ObjectMapper();

        try {

            String json = mapper.writeValueAsString(apiGatewayProxyRequestEvent);
            context.getLogger().log(json);
            
            Order order = mapper.readValue(apiGatewayProxyRequestEvent.getBody(), Order.class);
            order.id = UUID.randomUUID().toString();

            LambdaResponse<Order> resp = new LambdaResponse<Order>(0, "Success", order);

            return new APIGatewayProxyResponseEvent()
                    .withBody(mapper.writeValueAsString(resp))
                    .withStatusCode(200);

        } catch (JsonProcessingException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            context.getLogger().log(sw.toString());

            return new APIGatewayProxyResponseEvent()
                    .withBody("{\"errorCode\" : 400,\"message\" : \"Bad request\",\"data\" : null}")
                    .withStatusCode(400);

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            context.getLogger().log(sw.toString());

            return new APIGatewayProxyResponseEvent()
                    .withBody("{\"errorCode\" : 500,\"message\" : \"Internal server error\",\"data\" : null}")
                    .withStatusCode(500);
        }

    }
}
