package com.aws.lambda.api.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.aws.lambda.api.dto.LambdaResponse;
import com.aws.lambda.api.dto.Order;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ListOrderHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent>  {
    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent apiGatewayProxyRequestEvent, Context context) {
        ObjectMapper mapper = new ObjectMapper();
        String resource = apiGatewayProxyRequestEvent.getResource();
        List<Order> list = new ArrayList<Order>();

        try {

            switch (resource) {
                case "/order" -> {
                    // List all order
                    Order a = new Order(UUID.randomUUID().toString(), "Milk", 2);
                    list.add(a);
                    Order b = new Order(UUID.randomUUID().toString(), "Egg", 30);
                    list.add(b);
                }
                case "/order/{id}" -> {
                    // List order by id
                    String ordId = apiGatewayProxyRequestEvent.getPathParameters().get("id");
                    Order c = new Order(ordId, "Milk", 2);
                    list.add(c);
                }
            }

            LambdaResponse<Order> resp = new LambdaResponse<Order>(0, "Success", list);
            return new APIGatewayProxyResponseEvent()
                    .withBody(mapper.writeValueAsString(resp))
                    .withStatusCode(200);

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            context.getLogger().log(sw.toString());

            return new APIGatewayProxyResponseEvent()
                    .withBody("{\"errorCode\" : 500,\"message\" : \"Internal server error\",\"data\" : null}")
                    .withStatusCode(500);
        }

    }

}

